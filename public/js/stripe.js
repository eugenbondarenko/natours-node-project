/* eslint-disable */

import {loadStripe} from '@stripe/stripe-js'
import axios from 'axios'
import { showAlert } from './alerts';



export const bookTour = async (tourId) => {
    try {
        const stripe = await loadStripe('pk_test_51MWF7hDfjFdhXEaATrl0Jn7R2yyu0e50MV0rL7T3TYpnjpx8fpgWj5YZpjJycLXflwjilKdNuwDUdBhkJKSz22ce00JqHcM86K');
        const session = await axios(`/api/v1/bookings/checkout-session/${tourId}`);

        await stripe.redirectToCheckout({sessionId: session.data.session.id});
    } catch (error) {
        showAlert('error', error)
    }
    


};
