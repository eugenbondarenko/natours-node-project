const nodemailer = require('nodemailer');
const pug = require('pug');
const htmlToText = require('html-to-text');

module.exports = class Email {
  constructor(user, url) {
    this.to = user.email;
    this.firstName = user.name.split(' ')[0];
    this.url = url;
    this.from = `Eugen Bond <${process.env.EMAIL_FROM}>`;
  }

  newTransport() {
    if (process.env.NODE_ENV === 'production') {

      return nodemailer.createTransport({
        service: 'SendGrid',
        auth: {
          user: process.env.SEND_GRID_USER_NAME,
          pass: process.env.SEND_GRID_PASSWORD,
        },
      });
    }

    return nodemailer.createTransport({
      host: process.env.EMAIL_HOST,
      port: process.env.EMAIL_PORT,
      auth: {
        user: process.env.EMAIL_USERNAME,
        pass: process.env.EMAIL_PASSWORD,
      },
    });
  }

  async send(template, subject) {
    const html = pug.renderFile(`${__dirname}/../views/email/${template}.pug`, {
      firstName: this.firstName,
      url: this.url,
      subject,
    });

    const mailOptions = {
      from:
        process.env.NODE_ENV === 'production'
          ? process.env.SEND_GRID_EMAIL_FROM
          : this.from,
      to: this.to,
      subject,
      html,
      text: htmlToText.convert(html),
    };

    await this.newTransport().sendMail(mailOptions);
  }

  async sendWelcome() {
    this.send('welcome', 'Welcome to the club');
  }

  async sendPasswordReset() {
    this.send(
      'passwordReset',
      'Your password reset token (valid for only 10 minutes!)'
    );
  }
};
