const catchAsync = require('./catchAsync');
const AppError = require('./catchAsync');
const APIFeatures = require('./apiFeatures');

exports.deleteOne = (Model) =>
  catchAsync(async (req, res, next) => {
    const item = await Model.findByIdAndDelete(req.params.id);

    if (!item) {
      return next(new AppError('Item to delete not found', 404));
    }

    res.status(201).json({
      status: 'success',
      data: null,
    });
  });

exports.updateOne = (Model) =>
  catchAsync(async (req, res, next) => {
    const doc = await Model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true,
    });

    res.status(201).json({
      status: 'success',
      data: {
        data: doc,
      },
    });
  });

exports.createOne = (Model) =>
  catchAsync(async (req, res, next) => {
    const newDoc = await Model.create(req.body);

    res.status(201).json({
      status: 'success',
      newDoc,
    });
  });

exports.getOne = (Model, popOptions) =>
  catchAsync(async (req, res, next) => {
    let query = Model.findById(req.params.id);

    if (popOptions) query = query.populate(popOptions);
    const doc = await query;

    if (!doc) {
      return next(new AppError('No document with this ID', 404));
    }

    res.status(201).json({
      status: 'success',
      data: {
        data: doc,
      },
    });
  });

exports.getAll = (Model) =>
  catchAsync(async (req, res, next) => {
    //To allow for nested GET review on Tour (hack)
    let filters = {};
    if (req.params.tourId) filters = { tour: req.params.tourId };

    const features = new APIFeatures(Model.find(filters), req.query)
      .filter()
      .sort()
      .limit()
      .paging();
    const docs = await features.query;

    res.status(201).json({
      status: 'success',
      data: {
        docs,
      },
      results: docs.length,
    });
  });
