const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

const catchAsync = require('../utilities/catchAsync');
const TourModel = require('../models/tourModel');
const BookingModel = require('../models/bookingModel');
const UserModel = require('../models/userModel');
const AppError = require('../utilities/appError');
const factory = require('../utilities/factory');

exports.getCheckoutSession = catchAsync(async (req, res, next) => {
  const tour = await TourModel.findById(req.params.tourId);

  if (!tour) {
    return new AppError('There is no tour toy want');
  }

  const session = await stripe.checkout.sessions.create({
    payment_method_types: ['card'],
    success_url: `${req.protocol}://${req.get('host')}/my-tours`,
    cancel_url: `${req.protocol}://${req.get('host')}/tour/${tour.slug}`,
    customer_email: req.user.email,
    client_reference_id: req.params.tourId,
    mode: 'payment',
    line_items: [
      {
        price_data: {
          unit_amount: tour.price * 100,
          currency: 'usd',
          product_data: {
            name: `${tour.name} Tour`,
            description: tour.summary,
            images: [`https://www.natours.dev/img/tours/${tour.imageCover}`],
          },
        },
        quantity: 1,
      },
    ],
  });

  res.status(200).json({
    status: 'success',
    session,
  });
});

// exports.createBookingCheckout = catchAsync(async (req, res, next) => {
//   //Temp solution
//   const { user, tour, price } = req.query;

//   if (!user && !tour && !price) return next();
//   await BookingModel.create({ user, tour, price });

//   res.redirect(req.originalUrl.split('?')[0]);
// });

const createBookingCheckout = catchAsync(async (session) => {
  const user = await UserModel.findOne({ email: session.customer_email }).id;
  const tour = session.client_reference_id;
  const price = session.line_items[0].price_data.unit_amount / 100;

  await BookingModel.create({ user, tour, price });
});

exports.webhookCheckout = catchAsync(async (req, res, next) => {
  const signature = req.headers['stripe-signature'];

  let event;
  try {
    event = stripe.webhooks.constructEvent(
      req.body,
      signature,
      process.env.STRIPE_WEBHOOK_SECRET
    );
  } catch (error) {
    res.status(400).send(`Webhook error. ${error}`);
  }

  if (event.type === 'checkout.session.completed') {
    createBookingCheckout(event.data.object);
  }

  res.status(200).json({ received: true });
});

exports.deleteBooking = factory.deleteOne(BookingModel);
exports.updateBooking = factory.updateOne(BookingModel);
exports.getBooking = factory.getOne(BookingModel);
exports.getAllBookings = factory.getAll(BookingModel);
